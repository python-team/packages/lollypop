lollypop (1.4.41-1) unstable; urgency=medium

  * New upstream version 1.4.41
  * Update copyright year for Debian packaging to 2025

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 06 Feb 2025 00:40:25 +0100

lollypop (1.4.40-1) unstable; urgency=medium

  * New upstream version 1.4.40

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 17 Jun 2024 11:09:22 +0200

lollypop (1.4.39-1) unstable; urgency=medium

  * New upstream version 1.4.39
  * Update Standards Version to 4.7.0 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 14 Apr 2024 13:11:46 +0200

lollypop (1.4.38-1) unstable; urgency=medium

  * New upstream version 1.4.38
  * Upgrade Standards Version to 4.6.2 (No changes required)
  * Update copyright year for Debian packaging

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 03 Apr 2024 16:03:26 +0200

lollypop (1.4.37-1) unstable; urgency=medium

  * New upstream version 1.4.37
  * Remove patch to increase requirement on libsoup 3, applied upstream

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 21 Dec 2022 23:52:06 +0100

lollypop (1.4.36-2) unstable; urgency=medium

  * Upgrade depends to libsoup 3
  * Add patch to increase the required libsoup version
  * Recommend yt-dlp instead of youtube-dl (Closes: #1024217, #1004370)

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 01 Dec 2022 15:29:13 +0100

lollypop (1.4.36-1) unstable; urgency=medium

  * New upstream version 1.4.36

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 14 Nov 2022 17:41:31 +0100

lollypop (1.4.35-1) unstable; urgency=medium

  * New upstream version 1.4.35
  * Upgrade to Standards Version 4.6.1 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 17 Aug 2022 00:25:40 +0200

lollypop (1.4.34-1) unstable; urgency=medium

  * New upstream version 1.4.34

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 03 May 2022 15:08:00 +0200

lollypop (1.4.33-2) unstable; urgency=medium

  * source-only upload to allow migration to testing

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 16 Apr 2022 11:58:58 +0200

lollypop (1.4.33-1) unstable; urgency=medium

  * New upstream version 1.4.33

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 15 Apr 2022 18:29:38 +0200

lollypop (1.4.32-1) unstable; urgency=medium

  * New upstream version 1.4.32

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 14 Apr 2022 23:27:28 +0200

lollypop (1.4.31-1) unstable; urgency=medium

  * New upstream version 1.4.31

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 12 Apr 2022 13:40:52 +0200

lollypop (1.4.30-1) unstable; urgency=medium

  * New upstream version 1.4.30

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 19 Mar 2022 11:55:15 +0100

lollypop (1.4.29-1) unstable; urgency=medium

  * New upstream version 1.4.29

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 26 Jan 2022 01:58:50 +0100

lollypop (1.4.28-1) unstable; urgency=medium

  * New upstream version 1.4.28

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 16 Jan 2022 14:40:04 +0100

lollypop (1.4.27-1) unstable; urgency=medium

  * New upstream version 1.4.27
  * Update copyright year

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 12 Jan 2022 11:25:32 +0100

lollypop (1.4.26-1) unstable; urgency=medium

  * New upstream version 1.4.26

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 21 Dec 2021 15:32:42 +0100

lollypop (1.4.25-1) unstable; urgency=medium

  * New upstream version 1.4.25

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 12 Dec 2021 22:58:25 +0100

lollypop (1.4.24-1) unstable; urgency=medium

  * New upstream version 1.4.24
  * Update to Standards Version 4.6.0.1 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 22 Nov 2021 19:22:11 +0100

lollypop (1.4.23-1) unstable; urgency=medium

  * New upstream version 1.4.23

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 24 Aug 2021 20:49:53 +0200

lollypop (1.4.22-1) unstable; urgency=medium

  * Upload to unstable (Closes: #984942)

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 16 Aug 2021 01:28:18 +0200

lollypop (1.4.22-1~exp1) experimental; urgency=medium

  * New upstream version 1.4.22

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 05 Jul 2021 12:35:56 +0200

lollypop (1.4.20-1) experimental; urgency=medium

  * New upstream version 1.4.20
  * Remove patch
    0002-Fix-for-2778-that-keeps-the-miniplayer-visible-rathe.patch,
    alternative fix applied upstream

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 23 Jun 2021 20:17:31 +0200

lollypop (1.4.19-2) experimental; urgency=medium

  * Add patch to fix problems with miniplayer when passing the last song

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 25 May 2021 15:07:49 +0200

lollypop (1.4.19-1) experimental; urgency=medium

  * New upstream version 1.4.19

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 06 Apr 2021 16:02:57 +0200

lollypop (1.4.18-1) experimental; urgency=medium

  * New upstream version 1.4.18
  * d/salsa-ci: enable salsa ci pipelines
  * d/control: add Henry-Nicolas Tourneur in uploaders field
  * d/copyright: refresh years and add Henry-Nicolas Tourneur

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sat, 27 Mar 2021 20:16:14 +0000

lollypop (1.4.14-1) unstable; urgency=medium

  * New upstream version 1.4.14

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 18 Jan 2021 14:00:46 +0100

lollypop (1.4.13-1) unstable; urgency=medium

  * New upstream version 1.4.13

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 18 Jan 2021 12:41:41 +0100

lollypop (1.4.12-1) unstable; urgency=medium

  * New upstream version 1.4.12

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 18 Jan 2021 01:07:12 +0100

lollypop (1.4.11-1) unstable; urgency=medium

  * New upstream version 1.4.11

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 17 Jan 2021 13:05:16 +0100

lollypop (1.4.9-1) unstable; urgency=medium

  * New upstream version 1.4.9

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 11 Jan 2021 16:02:39 +0100

lollypop (1.4.8-1) unstable; urgency=medium

  * New upstream version 1.4.8

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 07 Jan 2021 15:20:46 +0100

lollypop (1.4.7-2) unstable; urgency=medium

  * Add depends on python3-gi-cairo

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 06 Jan 2021 20:57:54 +0100

lollypop (1.4.7-1) unstable; urgency=medium

  * New upstream version 1.4.7

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 19 Dec 2020 14:23:00 +0100

lollypop (1.4.5-2) unstable; urgency=medium

  * Add dependency on python3-cairo (Closes: #977566)
  * Upgrade to Standards Version 4.5.1 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 17 Dec 2020 01:09:32 +0100

lollypop (1.4.5-1) unstable; urgency=medium

  * New upstream version 1.4.5
  * Update patching version number (revision.sh changed)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 25 Oct 2020 14:30:30 +0100

lollypop (1.4.4-1) unstable; urgency=medium

  * New upstream version 1.4.4

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 24 Oct 2020 16:46:35 +0200

lollypop (1.4.3-1) unstable; urgency=medium

  * New upstream version 1.4.3

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 22 Oct 2020 15:07:48 +0200

lollypop (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 10 Oct 2020 12:24:28 +0200

lollypop (1.4.1-1) unstable; urgency=medium

  * New upstream version 1.4.1
  * Update copyright years

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 04 Oct 2020 14:14:27 +0200

lollypop (1.4.0-2) unstable; urgency=medium

  * Add depends on gir1.2-handy-1

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 24 Sep 2020 01:32:32 +0200

lollypop (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * PAPT and DPMT has merged - Update Vcs-* fields accordningly
  * Fix Maintainer field for Debian Python team
  * Add dep and build-dep on libhandy-1

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 23 Sep 2020 18:32:04 +0200

lollypop (1.3.6-1) unstable; urgency=medium

  * New upstream version 1.3.6

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 29 Aug 2020 17:27:29 +0200

lollypop (1.3.5-1) unstable; urgency=medium

  * New upstream version 1.3.5

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 16 Aug 2020 15:53:56 +0200

lollypop (1.3.4-2) unstable; urgency=medium

  * Add depends on python3-gst-1.0

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 14 Aug 2020 18:36:21 +0200

lollypop (1.3.4-1) unstable; urgency=medium

  * New upstream version 1.3.4

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 10 Aug 2020 14:10:30 +0200

lollypop (1.3.3-2) unstable; urgency=medium

  * Recommend youtube-dl (Closes: #967109)

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 04 Aug 2020 14:19:14 +0200

lollypop (1.3.3-1) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * New upstream version 1.3.3
  * Add Forwarded: not-needed to patch
    0001-Remove-getting-version-from-git-revision
  * Remove patch add_desktop_keywords_entry.patch, applied upstream
  * Delete debian manpage, instead install lollypop one

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 30 Jul 2020 20:19:21 +0200

lollypop (1.3.2-1) unstable; urgency=medium

  * New upstream version 1.3.2
  * Add Keywords to desktop file to silence lintian

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 28 Jun 2020 19:21:00 +0200

lollypop (1.3.1-1) unstable; urgency=medium

  * New upstream version 1.3.1
  * Use debhelper-compat 13
  * Fix duplicate-in-relation-field
  * Add Rules-Requires-Root: no
  * Add a ustream metadata file

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 09 Jun 2020 19:28:02 +0200

lollypop (1.3.0-1) unstable; urgency=medium

  * New upstream version 1.3.0
  * Remove pylast from dependencies

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 27 May 2020 12:54:24 +0200

lollypop (1.2.35-1) unstable; urgency=medium

  * New upstream version 1.2.35

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 17 Apr 2020 10:27:02 +0200

lollypop (1.2.34-1) unstable; urgency=medium

  * New upstream version 1.2.34

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 10 Apr 2020 16:40:02 +0200

lollypop (1.2.33-1) unstable; urgency=medium

  * New upstream version 1.2.33

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 04 Apr 2020 16:19:07 +0200

lollypop (1.2.32-1) unstable; urgency=medium

  * New upstream version 1.2.32

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 27 Mar 2020 23:05:54 +0100

lollypop (1.2.31-1) unstable; urgency=medium

  * New upstream version 1.2.31

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 27 Mar 2020 16:25:16 +0100

lollypop (1.2.30-1) unstable; urgency=medium

  * New upstream version 1.2.30

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 27 Mar 2020 12:36:28 +0100

lollypop (1.2.29-1) unstable; urgency=medium

  * New upstream version 1.2.29

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 20 Mar 2020 23:27:28 +0100

lollypop (1.2.27-1) unstable; urgency=medium

  * New upstream version 1.2.27

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 20 Mar 2020 17:35:45 +0100

lollypop (1.2.26-1) unstable; urgency=medium

  * New upstream version 1.2.26
  * Add patch to not get version from git revision

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 19 Mar 2020 22:10:00 +0100

lollypop (1.2.25-1) unstable; urgency=medium

  * New upstream version 1.2.25
  * Remove patch 0001-Patch-out-git-version-in-Debian-package.patch
    - no need due to upstream changes

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 11 Mar 2020 17:47:58 +0100

lollypop (1.2.24-1) unstable; urgency=medium

  * New upstream version 1.2.24

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 10 Mar 2020 19:39:46 +0100

lollypop (1.2.23-1) unstable; urgency=medium

  * New upstream version 1.2.23
  * Add patch to remove git revision from version (It doesn't work
    with a Debian packaging Git repository, and reports the version wrong)

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 25 Feb 2020 14:31:20 +0100

lollypop (1.2.22-1) unstable; urgency=medium

  * New upstream version 1.2.22

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 13 Feb 2020 16:40:43 +0100

lollypop (1.2.21-1) unstable; urgency=medium

  * New upstream version 1.2.21
  * Upgrade to Standards Version 4.5.0 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 12 Feb 2020 03:15:38 +0100

lollypop (1.2.20-1) unstable; urgency=medium

  * Update watch file for new file location
  * New upstream version 1.2.20

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 22 Jan 2020 17:25:36 +0100

lollypop (1.2.19-2) unstable; urgency=medium

  * Suggest gstreamer1.0-libav instead of depend on it (Closes: #947719)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 29 Dec 2019 17:58:15 +0100

lollypop (1.2.19-1) unstable; urgency=medium

  * New upstream version 1.2.19
  * debian/gbp.conf: Make sure we merge to debian/master branch

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 28 Dec 2019 17:32:23 +0100

lollypop (1.2.18-1) unstable; urgency=medium

  * New upstream version 1.2.18
  * Remove patch applied upstream:
    0001-Scanner-cans-emit-artist-updated-but-artist-may-have.patch

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 27 Dec 2019 00:25:55 +0100

lollypop (1.2.16-1) unstable; urgency=medium

  * New upstream version 1.2.16
  * Add patch cherry-picked from upstream fixing crash when updating
    song tags externally

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 29 Nov 2019 14:01:01 +0100

lollypop (1.2.15-1) unstable; urgency=medium

  * New upstream version 1.2.15

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 24 Nov 2019 14:03:39 +0100

lollypop (1.2.13-1) unstable; urgency=medium

  * New upstream version 1.2.13

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 17 Nov 2019 17:28:52 +0100

lollypop (1.2.12-1) unstable; urgency=medium

  * New upstream version 1.2.12

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 11 Nov 2019 15:23:54 +0100

lollypop (1.2.7-1) unstable; urgency=medium

  * New upstream version 1.2.7

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 08 Nov 2019 12:57:59 +0100

lollypop (1.2.5-1) unstable; urgency=medium

  * New upstream version 1.2.5

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 02 Nov 2019 20:24:39 +0100

lollypop (1.2.4-1) unstable; urgency=medium

  * New upstream version 1.2.4

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 01 Nov 2019 20:25:30 +0100

lollypop (1.2.3-1) unstable; urgency=medium

  * New upstream version 1.2.3
  * Add dependency on gir1.2-soup-2.4 (Closes: #943848)

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 30 Oct 2019 20:01:58 +0100

lollypop (1.2.2-1) unstable; urgency=medium

  * New upstream version 1.2.2

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 25 Oct 2019 23:39:06 +0200

lollypop (1.2.1-1) unstable; urgency=medium

  * Add dependency on gstreamer1.0-libav
  * New upstream version 1.2.1

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 22 Oct 2019 23:31:01 +0200

lollypop (1.2.0.1-1) unstable; urgency=medium

  * New upstream version 1.2.0.1
  * Remove patches
    0001-Check-genre-is-not-None,
    0002-No-more-args-for-DecadesBoxView-and-GenresBoxView
    - both applied upstream
  * Add dependency on gir1.2-gst-plugins-base-1.0

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 22 Oct 2019 19:55:05 +0200

lollypop (1.2.0-2) unstable; urgency=medium

  * Add patch to fix crash when updating tags when updating collection

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 21 Oct 2019 22:23:39 +0200

lollypop (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0
  * debian/patches/0001-Check-genre-is-not-None.patch
    - Add patch to check that genre is not None

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 21 Oct 2019 14:23:26 +0200

lollypop (1.1.99.9-1) experimental; urgency=medium

  * New upstream version 1.1.99.9

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 19 Oct 2019 00:10:35 +0200

lollypop (1.1.99.8-1) experimental; urgency=medium

  * New upstream version 1.1.99.8

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 15 Oct 2019 21:16:03 +0200

lollypop (1.1.99.7-1) experimental; urgency=medium

  * New upstream version 1.1.99.7

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 13 Oct 2019 21:50:10 +0200

lollypop (1.1.99.6-1) experimental; urgency=medium

  * New upstream version 1.1.99.6

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 09 Oct 2019 18:06:49 +0200

lollypop (1.1.99.5-1) experimental; urgency=medium

  * New upstream version 1.1.99.5

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 07 Oct 2019 19:02:17 +0200

lollypop (1.1.99.4-1) experimental; urgency=medium

  * New upstream version 1.1.99.4

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 07 Oct 2019 12:22:03 +0200

lollypop (1.1.99.2-1) experimental; urgency=medium

  * New upstream version 1.1.99.2

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 04 Oct 2019 01:01:07 +0200

lollypop (1.1.99.1-1) experimental; urgency=medium

  * New upstream version 1.1.99.1
  * Update Standards-Version to 4.4.1 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 03 Oct 2019 18:40:02 +0200

lollypop (1.1.98-1) experimental; urgency=medium

  * New upstream version 1.1.98
  * Add build-dep on libsoup2.4-dev

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 29 Sep 2019 19:08:42 +0200

lollypop (1.1.97.4-1) experimental; urgency=medium

  * New upstream version 1.1.97.4

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 28 Sep 2019 18:39:03 +0200

lollypop (1.1.97.3-1) experimental; urgency=medium

  * New upstream version 1.1.97.3
  * Remove path use_no_network_in_appstream_testing - applied upstream

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 27 Sep 2019 16:30:59 +0200

lollypop (1.1.4.16-4) unstable; urgency=medium

  * Fix python-gi-dev dependency

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 21 Sep 2019 14:42:11 +0200

lollypop (1.1.4.16-3) unstable; urgency=medium

  * Remove patch 0002-Remove-dependencies-not-needed-since-there-s-nothing.patch
  * Restore build dependencies (Closes: #939783)

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 19 Sep 2019 15:52:14 +0200

lollypop (1.1.4.16-2) unstable; urgency=medium

  * Fix dependencies (Closes: #939750)
  * Add patch to remove dependencies in meson.build (Closes: #939783)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 08 Sep 2019 21:11:48 +0200

lollypop (1.1.4.16-1) unstable; urgency=medium

  * New upstream version 1.1.4.16

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 04 Sep 2019 12:46:20 +0200

lollypop (1.1.4.15-2) unstable; urgency=medium

  * Add Architecture: All

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 03 Sep 2019 21:25:18 +0200

lollypop (1.1.4.15-1) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * New upstream version 1.1.4.15
  * Remove patch fix_crash_after_scan.patch, applied upstream

  [ Adrian Heine ]
  * Remove yelp-tools from deps

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 01 Sep 2019 17:37:52 +0200

lollypop (1.1.4.11-1) unstable; urgency=medium

  * Initial release (Closes: #847937)

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 02 Aug 2019 16:50:31 +0200
